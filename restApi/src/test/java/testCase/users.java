package testCase;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class users {
    @Test
    public void list_user (){
        String url = "https://reqres.in/api/users";


        RestAssured.baseURI = url;
        Response hasilResponse = (Response) ((ValidatableResponse)
                ((Response)
                        RestAssured
                                .given()
                                .urlEncodingEnabled(true)
                                .header("Content-Type", "application/json")
                                .get("/2"))
                                .then()).extract().response();

        int statusCode = hasilResponse.getStatusCode();
        System.out.println("Status code = " + hasilResponse.getStatusCode());
        Assert.assertEquals(statusCode, 200);

        JsonPath jsonPathEvaluator = hasilResponse.jsonPath();
        String firstName = (String) jsonPathEvaluator.get("data.first_name");
        System.out.println("Nama Depannya = " + firstName);

        System.out.println("type datanya = " + firstName.getClass().getSimpleName());
        int id = (int) jsonPathEvaluator.get("data.id");
        System.out.println("type datanya = " + ((Object)id).getClass().getSimpleName());

        System.out.println("Response Body = " + hasilResponse.asString());

    }
}
