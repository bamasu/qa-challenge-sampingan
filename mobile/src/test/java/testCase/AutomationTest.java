package testCase;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AutomationTest {

    private static AndroidDriver<MobileElement> driver;
    @BeforeClass
    public void setupAppium() throws MalformedURLException
    {
        String appiumServerURL = "http://127.0.0.1:4723/wd/hub";
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "com.sampingan.agentapp");
        dc.setCapability("appActivity", "com.sampingan.agentapp.activities.SplashActivity");
        dc.setCapability("automationName", "uiautomator2");
        driver = new AndroidDriver(new URL(appiumServerURL), dc);
    }
    @Test
    public void testAppium() {
        try {
            WebDriverWait wait;
// implicit
            driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

            MobileElement el1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.View/android.view.View/android.widget.ScrollView/android.view.View[1]");
            el1.click();
            MobileElement el2 = (MobileElement) driver.findElementById("com.sampingan.agentapp:id/editTextSearch");
            el2.sendKeys("Jawa Barat");
            MobileElement el3 = (MobileElement) driver.findElementById("com.sampingan.agentapp:id/textName");
            el3.click();
            MobileElement el4 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.View/android.view.View/android.widget.ScrollView/android.view.View[2]");
            el4.click();
            MobileElement el5 = (MobileElement) driver.findElementById("com.sampingan.agentapp:id/editTextSearch");
            el5.sendKeys("Bandung");
            MobileElement el6 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView");
            el6.click();
            MobileElement el7 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.View/android.view.View/android.view.View[2]/android.view.View");
            el7.click();
            Thread.sleep(3000);
            MobileElement hasilResult = (MobileElement)
                    driver.findElement(By.id("com.sampingan.agentapp:id/lbl_title")); if (hasilResult != null) {
                System.out.println("Berhasil pilih Domisili Bandung"); }
            System.out.println("Hai :"+ hasilResult.getText());

        } catch (Exception ex){

        }

    }
    @AfterClass
    public void tearDownAppium() {
    }
}
